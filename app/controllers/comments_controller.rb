class CommentsController < ApplicationController
  layout "dashboard"
  before_action :authenticate_user!
  access all: [:show], user:[:show, :destroy], site_admin: :all

  def index
    @comments = Comment.all
  end
  def create
   @video = Video.find(params[:video_id])
   @comment = @video.comments.build(comment_params)
   @comment.user = current_user
   if @comment.save
     flash[:success] = "Comment was created successfully"
     redirect_to pages_video_path(@video)
   else
     flash[:danger] = "Comment was not created"
     redirect_to :back
   end
 end

 def destroy
   @video = Video.find(params[:video_id])
   @comment = Comment.find(params[:id])
   @comment.destroy
    respond_to do |format|
     format.html { redirect_to comments_path }
   end
end

 private

 def comment_params
   params.require(:comment).permit(:description)
 end
end
