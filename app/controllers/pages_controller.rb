class PagesController < ApplicationController
  def home
    @video = Video.last
    @videos = Video.last(3).reverse
  end

  def video_all
    @videos = Video.all
  end
  def video
    @video = Video.find(params[:id])
    @comment = Comment.new
    @comments = @video.comments
  end
end
