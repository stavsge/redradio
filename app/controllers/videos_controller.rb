class VideosController < ApplicationController
before_action :set_video, only: [:edit, :update, :show, :destroy]
layout "dashboard"
access all: [:show], site_admin: :all
def index
  @videos = Video.all
end
def new
  @video = Video.new
end
def create
  @video = Video.new(video_params)
  respond_to do |format|
    if @video.save
      format.html { redirect_to videos_path, notice: 'Your Video item is now live' }
    else
      format.html { render :new }
    end
  end
end

def edit

end

def update
  respond_to do |format|
    if @video.update(video_params)
          format.html { redirect_to videos_path, notice: 'Your Video is updated' }
    else
      format.html { render :edit }
    end
  end
end

def show
  @comment = Comment.new
  @comments = @video.comments
end

def destroy
  @video.destroy
    respond_to do |format|
      format.html { redirect_to videos_path, notice: 'Video was successfully destroyed.' }
    end
end

private

  def set_video
    @video = Video.find(params[:id])
  end

  def video_params
    params.require(:video).permit(:title, :description, :video_url)
  end

end
