class Video < ApplicationRecord
  validates :title, :description, :video_url, presence: true
  has_many :comments, dependent: :destroy
end
