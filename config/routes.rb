Rails.application.routes.draw do

  root 'pages#home'
  get 'video/:id', to: 'pages#video', as: 'pages_video'
  get 'all-videos', to: 'pages#video_all', as: 'all_videos'
  devise_for :users,
              path: '',
              path_names: {sign_in: 'login', sign_out: 'logout', edit: 'profile', sign_up: 'registration'},
              controllers: {omniauth_callbacks: 'omniauth_callbacks'}


  resources :videos do
    resources :comments, only: [:create, :destroy]
  end
  resources :comments, only: [:index]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
